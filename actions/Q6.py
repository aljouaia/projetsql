import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
import pandas as pd
from utils import display
from utils.db import connect_db

class Window(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)

        # Définition de la taille de la fenêtre, du titre et des lignes/colonnes de l'affichage grid
        display.centerWindow(600, 400, self)
        self.title('Q6 : Graphique correlation temperatures minimales - coût de travaux (Isère / 2022)')
        display.defineGridDisplay(self, 2, 1)
        ttk.Label(self, text="""Pour l’Isère et l'année 2022, donner deux courbes sur le même graphique  :
   - par mois, l’évolution de la moyenne des températures minimales
   - par mois, l’évolution des totaux de coûts de travaux tout type confondu""",
                  wraplength=500, anchor="center", font=('Helvetica', '10', 'bold')).grid(sticky="we", row=0)

        temperature = """
            SELECT strftime('%m', date_mesure) as month,
                   AVG(temperature_min_mesure) as avg_min_temperature
            FROM Mesures
            WHERE strftime('%Y', date_mesure) = '2022'
            GROUP BY strftime('%m', date_mesure)
            ORDER BY month;
        """


        cout_total = """
            SELECT strftime('%m', date_x) as month,
                   SUM(cout_total_ht_travaux) as total_cost
            FROM Travaux
            WHERE strftime('%Y', date_x) = '2020'
            GROUP BY strftime('%m', date_x)
            ORDER BY month;
        """



        with connect_db() as connection:
            temperature_df = pd.read_sql_query(temperature, connection)
            total_cost_df = pd.read_sql_query(cout_total, connection)


        merged_df = pd.merge(temperature_df, total_cost_df, on='month', how='outer')

        fig, ax1 = plt.subplots()

        color = 'tab:red'
        ax1.set_xlabel('Mois')
        ax1.set_ylabel('la moyenne des températures minimales', color=color)
        ax1.plot(merged_df['month'], merged_df['avg_min_temperature'], color=color)
        ax1.tick_params(axis='y', labelcolor=color)

        ax2 = ax1.twinx()
        color = 'tab:blue'
        ax2.set_ylabel('Cout Total', color=color)
        ax2.plot(merged_df['month'], merged_df['total_cost'], color=color)
        ax2.tick_params(axis='y', labelcolor=color)

        plt.title('Graphique correlation temperatures minimales - coût de travaux (Isère / 2022)')
        plt.show()
