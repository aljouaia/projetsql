import tkinter
import tkinter as tk
from tkinter import ttk, messagebox, simpledialog
from utils import display
from utils.db import connect_db


class Window(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)

        # Définition de la taille de la fenêtre, du titre et des lignes/colonnes de l'affichage grid
        display.centerWindow(600, 400, self)
        self.title('Q7 : gérer les travaux de rénovation')
        display.defineGridDisplay(self, 2, 1)
        ttk.Label(self, text="""Proposer des fonctionnalités permettant de gérer l'ajout, modification et suppression pour un type de travaux""",
                  wraplength=500, anchor="center", font=('Helvetica', '10', 'bold')).grid(sticky="we", row=0)

        add_button = ttk.Button(self, text="Ajouter Travaux", command=self.add_travail)
        add_button.grid(row=1, column=1, pady=15)

        modify_button = ttk.Button(self, text="Modifier Travaux", command=self.modify_travail)
        modify_button.grid(row=2, column=0, pady=10)

        delete_button = ttk.Button(self, text="Supprimer Travaux", command=self.delete_travail)
        delete_button.grid(row=3, column=0, pady=10)

        self.search_var = tk.StringVar()
        ttk.Entry(self, textvariable=self.search_var).grid(sticky="we", row=1)
        ttk.Button(self, text="Search", command=self.search_travail).grid(sticky="we", row=2)






    def search_travail(self):
        travaux_id = self.search_var.get()

        existing_work_query = f"SELECT * FROM Travaux WHERE id_travaux = {travaux_id};"

        try:
            with connect_db() as connection:
                cursor = connection.cursor()
                cursor.execute(existing_work_query)
                existing_work = cursor.fetchone()
        except Exception as e:
            messagebox.showerror("Error", f"Error fetching data: {str(e)}")
            return


        if existing_work:
            response = messagebox.askquestion("Existing Work", f"Details for Travaux ID {travaux_id}:\n{existing_work}\nDo you want to modify this work?")
            if response == "yes":
                self.modify_travail(existing_work)
        else:
            messagebox.showinfo("Work Not Found", f"No work found for Travaux ID {travaux_id}")






    def add_travail(self):
        add_window = tk.Toplevel(self)
        add_window.title("Add New Travaux")

        labels = ["Cout Total HT Travaux", "Cout Induit HT Travaux", "Annee Travaux",
                  "Type Logement Travaux", "Annee Construction Travaux", "Code Region", "Code Departement"]

        entries = []
        for label in labels:
            tk.Label(add_window, text=label).pack()
            entry = tk.Entry(add_window)
            entry.pack()
            entries.append(entry)

        def insert_travail():
            new_values = [entry.get() for entry in entries]

            insert_query = """
                INSERT INTO Travaux (cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux,
                                      type_logement_travaux, annee_construction_travaux, code_region, code_departement)
                VALUES (?, ?, ?, ?, ?, ?, ?);
            """

            try:
                with connect_db() as connection:
                    cursor = connection.cursor()
                    cursor.execute(insert_query, new_values)
                    connection.commit()
                    messagebox.showinfo("Success", "New work added successfully")
                    add_window.destroy()
            except Exception as e:
                messagebox.showerror("Error", f"Error adding new work: {str(e)}")
            finally:
                connection.close()

        tk.Button(add_window, text="Add New Travaux", command=insert_travail).pack()







    def modify_travail(self, existing_work):
        modification_window = tk.Toplevel(self)
        modification_window.title("Modify Travaux")

        labels = ["Cout Total HT Travaux", "Cout Induit HT Travaux", "Annee Travaux",
                  "Type Logement Travaux", "Annee Construction Travaux", "Code Region", "Code Departement"]

        entries = []
        for label in labels:
            tk.Label(modification_window, text=label).pack()
            entry = tk.Entry(modification_window)
            entry.pack()
            entries.append(entry)

        for entry, value in zip(entries, existing_work[1:]):
            entry.insert(0, str(value))

        def update_travail():
            updated_values = [entry.get() for entry in entries]

            update_query = f"""
                UPDATE Travaux
                SET
                    cout_total_ht_travaux = ?,
                    cout_induit_ht_travaux = ?,
                    annee_travaux = ?,
                    type_logement_travaux = ?,
                    annee_construction_travaux = ?,
                    code_region = ?,
                    code_departement = ?
                WHERE id_travaux = ?;
            """

            try:
                with connect_db() as connection:
                    cursor = connection.cursor()
                    cursor.execute(update_query, updated_values + [existing_work[0]])
                    connection.commit()
                    messagebox.showinfo("Success", "Work updated successfully")
                    modification_window.destroy()
            except Exception as e:
                messagebox.showerror("Error", f"Error updating data: {str(e)}")


        tk.Button(modification_window, text="Update Travaux", command=update_travail).pack()








    def delete_travail(self):
        travaux_id = simpledialog.askinteger("Delete Travaux", "Enter id_travaux to delete:")

        if travaux_id is not None:
            confirm = messagebox.askyesno("Confirm Deletion",
                                          f"Do you want to delete Travaux with id_travaux {travaux_id}?")

            if confirm:
                delete_query = "DELETE FROM Travaux WHERE id_travaux = ?"

                try:
                    with connect_db() as connection:
                        cursor = connection.cursor()
                        cursor.execute(delete_query, (travaux_id,))
                        connection.commit()
                        messagebox.showinfo("Success", f"Travaux with id_travaux {travaux_id} deleted successfully")
                except Exception as e:
                    messagebox.showerror("Error", f"Error deleting Travaux: {str(e)}")
            else:
                messagebox.showinfo("Deletion Canceled", "Deletion of Travaux canceled by user.")
