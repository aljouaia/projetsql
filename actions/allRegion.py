#fonction qui affiche toutes les regions
import tkinter as tk
from utils import display
from tkinter import ttk

class Window(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)

        # Définition de la taille de la fenêtre, du titre et des lignes/colonnes de l'affichage grid
        display.centerWindow(600, 400, self)
        self.title('Display All Regions')
        display.defineGridDisplay(self, 2, 1)

        ttk.Label(self, text="Afficher toutes les régions.", wraplength=500, anchor="center", font=('Helvetica', '10', 'bold')).grid(sticky="we", row=0)

        # On définit les colonnes que l'on souhaite afficher dans la fenêtre et la requête
        columns = ('code_region', 'nom_region')
        query = """SELECT code_region, nom_region
                   FROM Regions
                   ORDER BY code_region"""

        # On utilise la fonction createTreeViewDisplayQuery pour afficher les résultats de la requête
        tree = display.createTreeViewDisplayQuery(self, columns, query, 200)
        tree.grid(sticky="nswe", row=1)

