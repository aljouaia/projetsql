create table Departements (
    code_departement TEXT,
    nom_departement TEXT,
    code_region INTEGER,
    zone_climatique TEXT,
    constraint pk_departements primary key (code_departement),
    constraint fk_region foreign key (code_region) references Regions(code_region)
);

create table Regions (
    code_region INTEGER,
    nom_region TEXT,
    constraint pk_regions primary key (code_region)
);

create table Mesures (
    code_departement TEXT,
    date_mesure DATE,
    temperature_min_mesure FLOAT,
    temperature_max_mesure FLOAT,
    temperature_moy_mesure FLOAT,
    constraint pk_mesures primary key (code_departement, date_mesure),
    constraint fk_mesures foreign key (code_departement) references Departements(code_departement)
);

-- Q4 Ajouter les créations des nouvelles tables
CREATE TABLE Communes (
    code_commune INTEGER,
    nom_commune TEXT,
    statut_commune TEXT,
    altitude_moyenne_commune INTEGER,
    population_commune INTEGER,
    superficie_commune INTEGER,
    code_canton_commune INTEGER,
    code_arrondissement_commune INTEGER,
    code_departement TEXT NOT NULL,
	CONSTRAINT pk_code_com_code_dep_communes PRIMARY KEY (code_commune, code_departement)
    CONSTRAINT fk_code_dep_comm FOREIGN KEY (code_departement) REFERENCES Departements (code_departement)
);


CREATE TABLE Travaux (
    id_travaux INTEGER PRIMARY KEY AUTOINCREMENT,
    cout_total_ht_travaux REAL,
    cout_induit_ht_travaux REAL,
    annee_travaux INTEGER,
    type_logement_travaux TEXT,
    annee_construction_travaux INTEGER,
    code_region INTEGER NOT NULL,
    code_departement INTEGER,
    date_x DATE,
    CONSTRAINT fk_code_region_travaux FOREIGN KEY (code_region) REFERENCES Regions (code_region),
    CONSTRAINT fk_code_departement_travaux FOREIGN KEY (code_departement) REFERENCES Departements (code_departement)
);

CREATE TABLE Isolations (
    id_travaux INTEGER PRIMARY KEY,
    poste_isolation TEXT CHECK(poste_isolation IN ('ITI', 'ITE', 'COMBLES PERDUES', 'PLANCHER BAS', 'RAMPANTS', 'SARKING', 'TOITURE TERRASSE')),
    isolant_isolation TEXT CHECK(isolant_isolation IS NULL OR isolant_isolation IN ('', 'LAINE MINERALE', 'LAINE VEGETALE', 'PLASTIQUES', 'AUTRES')),
    epaisseur_isolation INTEGER,
    surface_isolation REAL,
    cout_total_ht_travaux REAL,
    cout_induit_ht_travaux REAL,
    annee_travaux INTEGER,
    type_logement_travaux TEXT CHECK(type_logement_travaux IN ('COLLECTIF', 'INDIVIDUEL')),
    annee_construction_travaux INTEGER,
    code_region INTEGER NOT NULL,
    code_departement INTEGER,
    date_x DATE,
    CONSTRAINT fk_id_travaux_isolation FOREIGN KEY (id_travaux) REFERENCES Travaux(id_travaux) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_code_region_isolation FOREIGN KEY (code_region) REFERENCES Regions (code_region),
    CONSTRAINT fk_code_departement_isolation FOREIGN KEY (code_departement) REFERENCES Departements (code_departement)
);

CREATE TABLE Photovoltaiques (
    id_travaux INTEGER PRIMARY KEY,
    puissance_installee_photovoltaique INTEGER,
    type_panneaux_photovoltaique TEXT CHECK(type_panneaux_photovoltaique IN ('MONOCRISTALLIN', 'POLYCRISTALLIN')),
    cout_total_ht_travaux REAL,
    cout_induit_ht_travaux REAL,
    annee_travaux INTEGER,
    type_logement_travaux TEXT CHECK(type_logement_travaux IN ('COLLECTIF', 'INDIVIDUEL')),
    annee_construction_travaux INTEGER,
    code_region INTEGER NOT NULL,
    code_departement INTEGER,
    date_x DATE,
    CONSTRAINT fk_id_travaux_photovolt FOREIGN KEY (id_travaux) REFERENCES Travaux(id_travaux) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_code_reg_photovolt FOREIGN KEY (code_region) REFERENCES Regions (code_region),
    CONSTRAINT fk_code_dep_photovolt FOREIGN KEY (code_departement) REFERENCES Departements (code_departement)
);

CREATE TABLE Chauffages (
    id_travaux INTEGER PRIMARY KEY,
    energie_avant_travaux_chauffage TEXT CHECK ( energie_avant_travaux_chauffage IN ('AUTRES','BOIS','ELECTRICITE','FIOUL','GAZ') ),
    energie_installee_chauffage TEXT CHECK ( energie_installee_chauffage IN ('AUTRES','BOIS','ELECTRICITE','FIOUL','GAZ') ),
    generateur_chauffage TEXT CHECK ( generateur_chauffage IN ( 'AUTRE','CHAUDIERE','INSERT','PAC','POELE','RADIATEUR')) ,
    type_chaudiere_chauffage TEXT CHECK(type_chaudiere_chauffage IN ('STANDARD', 'AIR-EAU', 'AIR-AIR', 'A CONDENSATION', 'HPE', 'GEOTHERME', 'AUTRE')),
    cout_total_ht_travaux REAL,
    cout_induit_ht_travaux REAL,
    annee_travaux INTEGER,
    type_logement_travaux TEXT CHECK(type_logement_travaux IN ('COLLECTIF', 'INDIVIDUEL')),
    annee_construction_travaux INTEGER,
    code_region INTEGER NOT NULL,
    code_departement INTEGER,
    date_x DATE,
    CONSTRAINT fk_id_travaux_chauffage FOREIGN KEY (id_travaux) REFERENCES Travaux(id_travaux) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_code_reg_chauffage FOREIGN KEY (code_region) REFERENCES Regions (code_region),
    CONSTRAINT fk_code_dep_chauffage FOREIGN KEY (code_departement) REFERENCES Departements (code_departement)
);