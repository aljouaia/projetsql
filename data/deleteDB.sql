-- Suppression des tables liées aux dépendances
DROP TABLE IF EXISTS Isolations;
DROP TABLE IF EXISTS Photovoltaiques;
DROP TABLE IF EXISTS Chauffages;

-- Suppression des autres tables
DROP TABLE IF EXISTS Mesures;
DROP TABLE IF EXISTS Travaux;
DROP TABLE IF EXISTS Communes;
DROP TABLE IF EXISTS Departements;
DROP TABLE IF EXISTS Regions;
