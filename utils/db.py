import sqlite3
from sqlite3 import IntegrityError
import pandas

def connect_db():
    try:
        connection = sqlite3.connect("data/climat_france.db")
        connection.execute("PRAGMA foreign_keys = 1")
        return connection
    except Exception as e:
        print(f"Error connecting to the database: {e}")
        raise

# Pointeur sur la base de données
data = sqlite3.connect("data/climat_france.db")
data.execute("PRAGMA foreign_keys = 1")

# Fonction permettant d'exécuter toutes les requêtes sql d'un fichier
# Elles doivent être séparées par un point-virgule
def updateDBfile(data:sqlite3.Connection, file):

    # Lecture du fichier et placement des requêtes dans un tableau
    createFile = open(file, 'r')
    createSql = createFile.read()
    createFile.close()
    sqlQueries = createSql.split(";")

    # Exécution de toutes les requêtes du tableau
    cursor = data.cursor()
    for query in sqlQueries:
        cursor.execute(query)

# Action en cas de clic sur le bouton de création de base de données
def createDB():
    try:
        # On exécute les requêtes du fichier de création
        updateDBfile(data, "data/createDB.sql")
    except Exception as e:
        print ("L'erreur suivante s'est produite lors de la création de la base : " + repr(e) + ".")
    else:
        data.commit()
        print("Base de données créée avec succès.")

# En cas de clic sur le bouton d'insertion de données
#TODO Q4 Modifier la fonction insertDB pour insérer les données dans les nouvelles tables
def insertDB():
    try:
        # ? : paramètre de la requête qui doit être interprété comme une chaine de caractères dans l'insert
        # ?   : paramètre de la requête qui doit être interprété comme un nombre dans l'insert
        # la liste de noms en 3e argument de read_csv_file correspond aux noms des colonnes dans le CSV
        # ATTENTION : les attributs dans la BD sont généralement différents des noms de colonnes dans le CSV
        # Exemple : date_mesure dans la BD et date_obs dans le CSV

        # On ajoute les anciennes régions
        read_csv_file(
            "data/csv/Communes.csv", ';',
            "insert into Regions values (?,?)",
            ['Code Région', 'Région']
        )

        # On ajoute les nouvelles régions
        read_csv_file(
            "data/csv/AnciennesNouvellesRegions.csv", ';',
            "insert into Regions values (?,?)",
            ['Nouveau Code', 'Nom Officiel Région Majuscule']
        )

        # On ajoute les départements référencés avec les anciennes régions
        read_csv_file(
            "data/csv/Communes.csv", ';',
            "insert into Departements values (?,?, ?,'')",
            ['Code Département', 'Département', 'Code Région']
        )

        # On renseigne la zone climatique des départements
        read_csv_file(
            "data/csv/ZonesClimatiques.csv", ';',
            "update Departements set zone_climatique = ? where code_departement = ?",
            ['zone_climatique', 'code_departement']
        )

        # On modifie les codes région des départements pour les codes des nouvelles régions
        read_csv_file(
            "data/csv/AnciennesNouvellesRegions.csv", ';',
            "update Departements set code_region = ? where code_region = ?",
            ['Nouveau Code', 'Anciens Code']
        )

        # On supprime les anciennes régions, sauf si l'ancien code et le nouveau sont identiques (pour ne pas perdre les régions qui n'ont pas changé de code)
        read_csv_file(
            "data/csv/AnciennesNouvellesRegions.csv", ';',
            "delete from Regions where code_region = ? and ? <> ?",
            ['Anciens Code', 'Anciens Code', 'Nouveau Code']
        )
        print("Les erreurs UNIQUE constraint sont normales car on insère une seule fois les Regions et les Départemments")
        print("Insertion de mesures en cours...cela peut prendre un peu de temps")
        # On ajoute les mesures
        read_csv_file(
             "data/csv/Mesures.csv", ';',
             "insert into Mesures values (?,?, ?, ?, ?)",
             ['code_insee_departement', 'date_obs', 'tmin', 'tmax', 'tmoy']
        )

        read_csv_file(
            "data/csv/Communes.csv", ';',
            "insert into Communes (code_commune, nom_commune, statut_commune, altitude_moyenne_commune, population_commune, superficie_commune, code_canton_commune, code_arrondissement_commune, code_departement) values (?,?,?, ?, ?, ?, ?, ?, ? )",
            ['Code Commune', 'Commune', 'Statut', 'Altitude Moyenne', 'Population', 'Superficie', 'Code Canton',
             'Code Arrondissement', 'Code Département']
        )

        #insertions dans traveaux et dans Isolations

        travaux_query_isolations = "INSERT INTO Travaux (cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux, type_logement_travaux, annee_construction_travaux, code_region, code_departement, date_x) VALUES (?,?,?,?,?,?,?, ?)"
        travaux_columns_isolations = ['cout_total_ht', 'cout_induit_ht', 'annee_travaux', 'type_logement',
                                      'annee_construction', 'code_region', 'code_departement', 'date_x']

        isolations_query = """
            INSERT INTO Isolations (
                id_travaux, poste_isolation, isolant_isolation, epaisseur_isolation,
                surface_isolation, cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux,
                type_logement_travaux, annee_construction_travaux, code_region, code_departement, date_x
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """
        isolations_columns = ['poste_isolation', 'isolant_isolation', 'epaisseur_isolation', 'surface_isolation',
                              'cout_total_ht', 'cout_induit_ht', 'annee_travaux', 'type_logement', 'annee_construction',
                              'code_region', 'code_departement', 'date_x']

        read_csv_file2("data/csv/Isolation.csv", ';', 'Isolations', travaux_query_isolations,
                       travaux_columns_isolations, isolations_query, isolations_columns)

        # insertions dans traveaux et dans Photovoltaiques

        travaux_query_photovoltaiques = "INSERT INTO Travaux (cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux, type_logement_travaux, annee_construction_travaux, code_region, code_departement, date_x) VALUES (?,?,?,?,?,?,?, ?)"
        travaux_columns_photovoltaiques = ['cout_total_ht', 'cout_induit_ht', 'annee_travaux', 'type_logement',
                                           'annee_construction', 'code_region', 'code_departement', 'date_x']

        photovoltaiques_query = """
            INSERT INTO Photovoltaiques (
                id_travaux, puissance_installee_photovoltaique, type_panneaux_photovoltaique,
                cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux, type_logement_travaux,
                annee_construction_travaux, code_region, code_departement, date_x
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """
        photovoltaiques_columns = ['puissance_installee', 'type_panneaux', 'cout_total_ht', 'cout_induit_ht',
                                   'annee_travaux', 'type_logement', 'annee_construction', 'code_region',
                                   'code_departement', 'date_x']

        read_csv_file2("data/csv/Photovoltaique.csv", ';', 'Photovoltaiques', travaux_query_photovoltaiques,
                       travaux_columns_photovoltaiques, photovoltaiques_query, photovoltaiques_columns)


        # insertions dans traveaux et dans Chauffages

        travaux_query_chauffages = "INSERT INTO Travaux (cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux, type_logement_travaux, annee_construction_travaux, code_region, code_departement, date_x) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
        travaux_columns_chauffages = ['cout_total_ht', 'cout_induit_ht', 'annee_travaux', 'type_logement',
                                      'annee_construction', 'code_region', 'code_departement', 'date_x']

        chauffages_query = "INSERT INTO Chauffages (id_travaux, energie_avant_travaux_chauffage, energie_installee_chauffage, generateur_chauffage, type_chaudiere_chauffage, cout_total_ht_travaux, cout_induit_ht_travaux, annee_travaux, type_logement_travaux, annee_construction_travaux, code_region, code_departement, date_x) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        chauffages_columns = ['energie_chauffage_avt_travaux', 'energie_chauffage_installee', 'generateur',
                              'type_chaudiere', 'cout_total_ht', 'cout_induit_ht', 'annee_travaux', 'type_logement',
                              'annee_construction', 'code_region', 'code_departement', 'date_x']

        read_csv_file2("data/csv/Chauffage.csv", ';', 'Chauffages',
                       travaux_query_chauffages, travaux_columns_chauffages,
                       chauffages_query, chauffages_columns)









    except Exception as e:
        print ("L'erreur suivante s'est produite lors de l'insertion des données : " + repr(e) + ".")
    else:
        data.commit()
        print("Un jeu de test a été inséré dans la base avec succès.")

# En cas de clic sur le bouton de suppression de la base
def deleteDB():
    try:
        updateDBfile(data, "data/deleteDB.sql")
    except Exception as e:
        print ("L'erreur suivante s'est produite lors de la destruction de la base : " + repr(e) + ".")
    else:
        data.commit()
        print("La base de données a été supprimée avec succès.")

def read_csv_file(csvFile, separator, query, columns):
    # Lecture du fichier CSV csvFile avec le séparateur separator
    # pour chaque ligne, exécution de query en la formatant avec les colonnes columns
    df = pandas.read_csv(csvFile, sep=separator)
    df = df.where(pandas.notnull(df), 'null')

    cursor = data.cursor()
    for ix, row in df.iterrows():
        try:
            tab = []
            for i in range(len(columns)):
                # pour échapper les noms avec des apostrophes, on remplace dans les chaines les ' par ''
                if isinstance(row[columns[i]], str):
                    row[columns[i]] = row[columns[i]].replace("'","''")
                tab.append(row[columns[i]])

            formatedQuery = query.format(*tab)

            # On affiche la requête pour comprendre la construction ou débugger !
            #print(formatedQuery)
            print(formatedQuery)
            cursor.execute(query, tuple(tab))
        except IntegrityError as err:
            print(err)


def read_csv_file2(csvFile, separator, table_name, travaux_query, travaux_columns, additional_query, additional_columns):
    df = pandas.read_csv(csvFile, sep=separator)
    df = df.where(pandas.notnull(df), None)

    cursor = data.cursor()

    for ix, row in df.iterrows():
        try:
            # Insert into Travaux table
            travaux_values = []

            for i in range(len(travaux_columns)):
                if isinstance(row[travaux_columns[i]], str):
                    row[travaux_columns[i]] = row[travaux_columns[i]].replace("'", "''")
                travaux_values.append(row[travaux_columns[i]])

            cursor.execute(travaux_query, tuple(travaux_values))

            # Get the last inserted ID from Travaux table
            generated_id = cursor.lastrowid

            additional_values = [
                generated_id,
                *map(lambda col: row.get(col, None), additional_columns)
            ]

            cursor.execute(additional_query, tuple(additional_values))

            print("Success")

        except IntegrityError as err:
            print(err)




